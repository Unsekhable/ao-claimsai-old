package com.apporchid.claims.ui.builder;

import com.apporchid.claims.constants.IClaimsAISolutionConstants;
import com.apporchid.vulcanux.config.builder.BaseAppConfigurationBuilder;

public abstract class BaseClaimsAIAppsBuilder extends BaseAppConfigurationBuilder implements IClaimsAISolutionConstants{

	public BaseClaimsAIAppsBuilder() {
		super(DEFAULT_DOMAIN_ID, DEFAULT_SUB_DOMAIN_ID);
	}

	@Override
	public Boolean isAppVisibleInAppstore(String appId) {
		return Boolean.FALSE;
	}
	
}

