package com.apporchid.claims.ui.builder;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.apporchid.claims.constants.IClaimsAISolutionConstants;
import com.apporchid.claims.pipeline.builder.ClaimsPipelineBuilder;
import com.apporchid.config.builder.BaseConfigurationBuilder;
import com.apporchid.core.common.UIDUtils;
import com.apporchid.foundation.ui.config.solution.ISolutionConfig;
import com.apporchid.foundation.ui.config.solution.ISolutionPageConfig;
import com.apporchid.vulcanux.config.builder.BaseSolutionConfigurationBuilder;

@Component
public class ClaimsAISolutionBuilder extends BaseSolutionConfigurationBuilder implements IClaimsAISolutionConstants{

	public ClaimsAISolutionBuilder() {
		super(DEFAULT_DOMAIN_ID, DEFAULT_SUB_DOMAIN_ID);
	}

	@Override
	protected List<Class<? extends BaseConfigurationBuilder<?>>> getDependentConfigBuilders() {
		List<Class<? extends BaseConfigurationBuilder<?>>> builders = new ArrayList<>();

		builders.add(ClaimsPipelineBuilder.class);
		builders.add(ClaimsAIAppsBuilder.class);
		

		return builders;
	}

	@Override
	protected ISolutionConfig getSolution() {
		List<ISolutionPageConfig> solutionPages = new ArrayList<>();
		ISolutionPageConfig suggestionPage = getSolutionPageConfig("welcomePage", "Welcome", true, "",
				toApplicationReferences(getApplicationIds()));
		solutionPages.add(suggestionPage);
		
		/*ISolutionPageConfig suggestionPage = getSolutionPageConfig("claimChangeLog", "Claim Change Log", true, "vuxicon-table",
				toApplicationReferences(getApplicationIds()));
		solutionPages.add(suggestionPage);
		ISolutionPageConfig suggestionPage1 = getSolutionPageConfig("claimContactReserveFeatureHistoryGrid", "claim Contact Reserve Feature History ", true, "vuxicon-table",
				toApplicationReferences(getclaimContactReserveApplicationIds()));
		solutionPages.add(suggestionPage1);
		
		ISolutionPageConfig suggestionPage2 = getSolutionPageConfig("ClaimContactGrid", "claim Contact Grid ", true, "vuxicon-table",
				toApplicationReferences(getClaimContactGridApplicationIds()));
		solutionPages.add(suggestionPage2);
		ISolutionPageConfig suggestionPage3 = getSolutionPageConfig("ClaimGrid", "claim Grid", true, "vuxicon-table",
				toApplicationReferences(getClaimGridApplicationIds()));
		solutionPages.add(suggestionPage3);
		ISolutionPageConfig suggestionPage4 = getSolutionPageConfig("ClaimPaymentGrid", "claim Payment ", true, "vuxicon-table",
				toApplicationReferences(getClaimPaymentGridApplicationIds()));
		solutionPages.add(suggestionPage4);
		ISolutionPageConfig suggestionPage5 = getSolutionPageConfig("ClaimVehicleCoverageHistory", "claim Vehicle Coverage History ", true, "vuxicon-table",
				toApplicationReferences(getClaimVehicleCoverageHistoryApplicationIds()));
		solutionPages.add(suggestionPage5);
		ISolutionPageConfig suggestionPage6 = getSolutionPageConfig("ClaimVendors", "claim Vendors", true, "vuxicon-table",
				toApplicationReferences(getClaimVendorsApplicationIds()));
		solutionPages.add(suggestionPage6);
		ISolutionPageConfig suggestionPage7 = getSolutionPageConfig("ContactGrid", "Contract Grid ", true, "vuxicon-table",
				toApplicationReferences(getContactGridApplicationIds()));
		solutionPages.add(suggestionPage7);
		ISolutionPageConfig suggestionPage8 = getSolutionPageConfig("Features", "Features ", true, "vuxicon-table",
				toApplicationReferences(getFeaturesApplicationIds()));
		solutionPages.add(suggestionPage8);
		ISolutionPageConfig suggestionPage9 = getSolutionPageConfig("VendorGrid", "Vendor Grid ", true, "vuxicon-table",
				toApplicationReferences(getVendorGridReserveApplicationIds()));
		solutionPages.add(suggestionPage9);*/
		
	ClaimsAISolutionHeaderConfig headerConfig = new ClaimsAISolutionHeaderConfig.Builder().withId(SOLUTION_ID+"Id").withCustomLogoPath(SOLUTION_LOGO)
				.showSolutionName(Boolean.FALSE).build();

		return createSolution(SOLUTION_ID, "ClaimsAI", false, true, solutionPages, headerConfig,SOLUTION_ICON);

	}
	
	protected String[] getApplicationIds() {
		return new String[] { "messageapp" };
	}
	
	/*private String[] getVendorGridReserveApplicationIds() {
		return new String[] { "VendorGridApp" };
	}

	private String[] getFeaturesApplicationIds() {
		return new String[] { "FeaturesApp" };
	}

	private String[] getContactGridApplicationIds() {
		return new String[] { "ContactGridApp" };
	}

	private String[] getClaimVendorsApplicationIds() {
		return new String[] { "ClaimVendorsApp" };
	}

	private String[] getClaimVehicleCoverageHistoryApplicationIds() {
		return new String[] { "ClaimVehicleCoverageHistoryApp" };
	}

	private String[] getClaimPaymentGridApplicationIds() {
		return new String[] { "ClaimPaymentGridApp" };
	}

	private String[] getClaimGridApplicationIds() {
		return new String[] { "ClaimGridApp" };
	}

	private String[] getClaimContactGridApplicationIds() {
		return new String[] { "claimContactGridApp" };
	}

	protected String[] getApplicationIds() {
		return new String[] { "claimchangelogapp" };
	}

	protected String[] getclaimContactReserveApplicationIds() {
		return new String[] { "ClaimContactReserveFeatureHistoryapp" };
	} */
	
	@Override
	protected String[] getSidebarChildIds(String parentId) {
		return super.getSidebarChildIds(parentId);
	}

	@Override
	protected String getSidebarIcon(String id) {
		return super.getSidebarIcon(id);
	}

	@Override
	protected int getNoOfColumns(String solutionPageId) {
			return super.getNoOfColumns(solutionPageId);
	}

	@Override
	protected double getRowspan(String solutionPageId, String applicationId, int noOfRows, int noOfColumns) {
		return super.getRowspan(solutionPageId, applicationId, noOfRows, noOfColumns);
	}

	@Override
	protected double getColspan(String solutionPageId, String applicationId, int noOfRows, int noOfColumns) {
		return super.getColspan(solutionPageId, applicationId, noOfRows, noOfColumns);
	}

	@Override
	protected double getRowIndex(String solutionPageId, String applicationId) {
		return super.getRowIndex(solutionPageId, applicationId);
	}

	@Override
	public String getSolutionId() {
		return UIDUtils.getUID(domainId, subDomainId, SOLUTION_ID);
	}

	@Override
	protected String[] getJsFiles() {
		return new String[] { "js/claimsSolutionUtils.js" };
	}

	@Override
	protected String[] getCssFiles() {
		return new String[] { "css/claims.css" };
	}

}
