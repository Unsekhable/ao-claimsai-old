package com.apporchid.claims.ui.builder;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.apporchid.foundation.ui.config.IUIContainerConfig;
import com.apporchid.foundation.ui.config.application.IApplicationConfig;
import com.apporchid.foundation.ui.config.microapp.IMicroApplicationConfig;
import com.apporchid.vulcanux.common.ui.config.UIContainerConfig;
import com.apporchid.vulcanux.common.ui.config.layout.RowsColsLayoutConfig;
import com.apporchid.vulcanux.ui.config.dataview.SimpleDataViewConfig;
import com.apporchid.vulcanux.ui.config.dataview.data.DataViewDataConfig;
import com.apporchid.vulcanux.ui.config.table.DataTableColumnConfig;
import com.apporchid.vulcanux.ui.config.table.SimpleDataTableConfig;
import com.apporchid.vulcanux.ui.config.table.data.DataTableDataConfig;

@Component
public class ClaimsAIAppsBuilder extends BaseClaimsAIAppsBuilder{

	@Override
	protected List<IApplicationConfig> getApplications() {
		IApplicationConfig[] selectControlsApplications = new IApplicationConfig[] {
				getApplication("messageapp", getMessage()),
				getApplication("claimchangelogapp", getClaimChangeLogMicroApp()),
				getApplication("ClaimContactReserveFeatureHistoryapp", claimContactReserveFeatureHistoryGridMicroApp()),
				
				getApplication("claimContactGridApp", claimContactGridMicroApp()),
				getApplication("ClaimGridApp", claimGridMicroApp()),
				getApplication("ClaimPaymentGridApp", claimPaymentGridMicroApp()),
				getApplication("ClaimVehicleCoverageHistoryApp", claimVehicleCoverageHistoryMicroApp()),
				getApplication("ClaimVendorsApp", claimVendorsMicroApp()),
				getApplication("ContactGridApp", contactGridMicroApp()),
				getApplication("FeaturesApp", featuresMicroApp()),
				getApplication("VendorGridApp", vendorGridMicroApp())
				};
		return Arrays.asList(selectControlsApplications);
	}

	
	private IMicroApplicationConfig<?> vendorGridMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("VendorGridPipeline")).build();
		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("Vendor Grid").withDataConfig(dataConfig).build();
		return dataTable;
	}


	private IMicroApplicationConfig<?> featuresMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("FeaturesPipeline")).build();
		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("Features").withDataConfig(dataConfig).build();
		return dataTable;
	}


	private IMicroApplicationConfig<?> contactGridMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("ContactGridPipeline")).build();
		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("Contact Grid").withDataConfig(dataConfig).build();
		return dataTable;
	}


	private IMicroApplicationConfig<?> claimVendorsMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("ClaimVendorsPipeline")).build();
		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("Claim  Vendors").withDataConfig(dataConfig).build();
		return dataTable;
	}


	private IMicroApplicationConfig<?> claimVehicleCoverageHistoryMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("ClaimVehicleCoverageHistoryPipeline")).build();
		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("Claim Vehicle Coverage Payment Grid").withDataConfig(dataConfig).build();
		return dataTable;
	}


	private IMicroApplicationConfig<?> claimPaymentGridMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("ClaimPaymentGridPipeline")).build();
		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("Claim Payment Grid").withDataConfig(dataConfig).build();
		return dataTable;
	}


	private IMicroApplicationConfig<?> claimGridMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("ClaimGridPipeline")).build();
		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("Claim Grid").withDataConfig(dataConfig).build();
		return dataTable;
	}


	private IMicroApplicationConfig<?> claimContactGridMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("ClaimContactGridPipeline")).build();
		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("Claim Contact Grid").withDataConfig(dataConfig).build();
		return dataTable;
	}


	private IMicroApplicationConfig<?> claimContactReserveFeatureHistoryGridMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("ClaimContactReserveFeatureHistoryPipeline")).build();
		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("Claim Contact Reserve Feature History Grid").withDataConfig(dataConfig).build();
		return dataTable;
	}


	protected IUIContainerConfig<?>  getMessage() {
		RowsColsLayoutConfig.Builder rowsColsLayoutDataview = new RowsColsLayoutConfig.Builder().useRows(false);
		
		DataViewDataConfig listdataConfig = new DataViewDataConfig.Builder()
				.pipelineId(getId("MessagePipeline"))
				.build();
		SimpleDataViewConfig dataConfig=new SimpleDataViewConfig.Builder().withId("dataConfigId")
				.withTemplate(getJSFunction("claimsSolutionUtils", "messageTemplate"))
				.withDataConfig(listdataConfig).build();
		
		rowsColsLayoutDataview.addComponentLayoutProperties(dataConfig);
		UIContainerConfig containerConfig = new UIContainerConfig.Builder().addComponent(dataConfig)
				.withLayout(rowsColsLayoutDataview.build()).build();
		return containerConfig;
	}
	
	protected IMicroApplicationConfig<?> getClaimChangeLogMicroApp() {
		DataTableDataConfig dataConfig = new DataTableDataConfig.Builder().pipelineId(getId("GetClaimChangeLogPipeline"))
				.build();
		DataTableColumnConfig clamChangeLogIdColumnConfig = new DataTableColumnConfig.Builder().id("claimChangeLogId").build();
		DataTableColumnConfig fieldNameColumnConfig = new DataTableColumnConfig.Builder().id("fieldName").build();
		DataTableColumnConfig claimIdColumnConfig = new DataTableColumnConfig.Builder().id("claimId").width(200).build();
		DataTableColumnConfig userFullNameColumnConfig = new DataTableColumnConfig.Builder().id("userFullName").build();
		DataTableColumnConfig userIdColumnConfig = new DataTableColumnConfig.Builder().id("userId").build();
		DataTableColumnConfig clientClaimNumberColumnConfig = new DataTableColumnConfig.Builder().id("clientClaimNumber").build();
		DataTableColumnConfig claimChangedLogDateColumnConfig = new DataTableColumnConfig.Builder().id("claimChangedLogDate").build();
		

		SimpleDataTableConfig dataTable = new SimpleDataTableConfig.Builder().withTitle("ClaimChangeLog Templates")
				.column(clamChangeLogIdColumnConfig).column(fieldNameColumnConfig).column(claimIdColumnConfig)
				.column(userFullNameColumnConfig).column(userIdColumnConfig)
				.column(clientClaimNumberColumnConfig).column(claimChangedLogDateColumnConfig)
				//.showOnlyConfiguredColumns(true)
				.withDataConfig(dataConfig).build();

		return dataTable;
	}
}
