package com.apporchid.claims.ui.builder;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;

import com.apporchid.foundation.common.property.IConfigProperty;
import com.apporchid.foundation.ui.EUIControlType;
import com.apporchid.foundation.ui.ICSSConstants;
import com.apporchid.foundation.ui.config.IUIComponentReferenceConfig;
import com.apporchid.foundation.ui.config.IUIControlConfig;
import com.apporchid.foundation.ui.config.microapp.IMicroApplicationConfig;
import com.apporchid.vulcanux.common.ui.config.UIContainerConfig;
import com.apporchid.vulcanux.common.ui.config.layout.ComponentRowsColsLayoutPropertiesConfig;
import com.apporchid.vulcanux.common.ui.config.layout.RowsColsLayoutConfig;
import com.apporchid.vulcanux.ui.config.solution.BaseSolutionHeaderConfig;
import com.apporchid.vulcanux.ui.config.solution.SolutionBurgerMenuConfig;
import com.apporchid.vulcanux.ui.config.solution.SolutionProfileMenuConfig;
import com.apporchid.vulcanux.ui.config.uicontrol.ImageConfig;
import com.apporchid.vulcanux.ui.config.uicontrol.LabelConfig;
import com.apporchid.vulcanux.ui.config.uicontrol.SpacerConfig;
import com.apporchid.vulcanux.utils.IVuxConstants;

public class ClaimsAISolutionHeaderConfig extends BaseSolutionHeaderConfig{

	private static final long serialVersionUID = 5169447521643787649L;

	public ClaimsAISolutionHeaderConfig() {
		super();
	}

	public static final class Builder extends BaseSolutionHeaderConfig.Builder<ClaimsAISolutionHeaderConfig, Builder> {

		@Override
		protected ClaimsAISolutionHeaderConfig create() {
			return new ClaimsAISolutionHeaderConfig();
		}

		@Override
		protected Builder getBuilder() {
			return this;
		}

	}
	@Override
	protected UIContainerConfig getDefaultHeaderContainer() {
		List<IUIComponentReferenceConfig> componentsList = new ArrayList<>();
		int headerHeight = getHeaderHeight();
		if (headerHeight <= 0) {
			headerHeight = getHeaderHeight();
		}
		RowsColsLayoutConfig.Builder toolbarConfigbuilder = new RowsColsLayoutConfig.Builder().withHeight(headerHeight)
				.withResponsive("hide");

		// Left Menu Items
		SolutionBurgerMenuConfig solutionBurgerMenuComponent = getSolutionBurgerMenuComponent();
		LabelConfig burgerMenuSeperatorComponent = getSeperatorComponent("solBurgerMenuSeperator",
				"sep_border solution_list_separator");
		ImageConfig companyLogoComponent = getCompanyLogoComponent();
		LabelConfig logoSeperatorComponent = getSeperatorComponent("solLogoSeperator", "sep_border");
		ImageConfig solutionLogoComponent = getSolutionLogoComponent();
		LabelConfig solutionNameComponent = getSolutionNameComponent();
		// Spacer
		SpacerConfig spacerComponent = getSpacerComponent("spacerHeader");
		// Right side menu items
		IUIControlConfig searchSeperatorComponent = getSeperatorComponent("searchSeperator",
				"sep_border search_separator");
		IMicroApplicationConfig<?> searchbarComponent = getSearchbarComponent();
		IUIControlConfig addlContainerSeperator = getSeperatorComponent("addlContainerSeperator",
				"sep_border search_separator");
		IUIControlConfig profileSeperatorComponent = getSeperatorComponent("profileSeperator",
				"sep_border profile_separator");
		SolutionProfileMenuConfig userPofileMenuComponent = getUserPofileMenuComponent();
		IUIControlConfig notificationComponent = getNotificationComponent();

		// Add Header Components to Layout & Components List
		// Left menu items
		componentsList.add(solutionBurgerMenuComponent);
		toolbarConfigbuilder.addComponentLayoutProperties(solutionBurgerMenuComponent, 50, 0, false);

		componentsList.add(burgerMenuSeperatorComponent);
		toolbarConfigbuilder.addComponentLayoutProperties(burgerMenuSeperatorComponent, 1, 0, false);

		if (BooleanUtils.isTrue(getShowSolutionIcon().getValue())) {
			componentsList.add(solutionLogoComponent);
			toolbarConfigbuilder.addComponentLayoutProperties(solutionLogoComponent, 147, 0);
		}

		if (BooleanUtils.isTrue(isShowSolutionName().getValue())) {
			componentsList.add(solutionNameComponent);
			toolbarConfigbuilder.addComponentLayoutProperties(solutionNameComponent);
		}

		if (BooleanUtils.isTrue(getShowSolutionIcon().getValue())
				|| BooleanUtils.isTrue(isShowSolutionName().getValue())) {
			componentsList.add(logoSeperatorComponent);
			toolbarConfigbuilder.addComponentLayoutProperties(logoSeperatorComponent, 1, 0);
		}

		if (BooleanUtils.isTrue(isShowCompanyLogo().getValue())) {
			componentsList.add(companyLogoComponent);
			toolbarConfigbuilder.addComponentLayoutProperties(companyLogoComponent, 50, 0);
		}

		// Spacer
		componentsList.add(spacerComponent);
		toolbarConfigbuilder.addComponentLayoutProperties(spacerComponent, 0, 0, 0.01, false);

		// Right side menu items
		if (isShowSearchbar().getValue()) {
			componentsList.add(searchbarComponent);
			componentsList.add(searchSeperatorComponent);

			toolbarConfigbuilder.addComponentLayoutProperties(searchbarComponent.getId(),
					new ComponentRowsColsLayoutPropertiesConfig.Builder().withMinWidth(200).withMaxWidth(600).build());

			toolbarConfigbuilder.addComponentLayoutProperties(searchSeperatorComponent, 1, 0);
		}

		if (getAdditionalContainer() != null) {
			componentsList.addAll(getAdditionalContainer().getComponents());
			componentsList.add(addlContainerSeperator);

			List<IUIComponentReferenceConfig> additionalComponents = getAdditionalContainer().getComponents();
			for (IUIComponentReferenceConfig component : additionalComponents) {
				IConfigProperty<?> widthValue = component.getProperty(ICSSConstants.WIDTH);
				Integer width = widthValue != null ? (Integer) widthValue.getValue() : 0;

				IConfigProperty<?> heightValue = component.getProperty(ICSSConstants.HEIGHT);
				Integer height = heightValue != null ? (Integer) heightValue.getValue() : 0;

				toolbarConfigbuilder.addComponentLayoutProperties(component, width, height);
			}
			toolbarConfigbuilder.addComponentLayoutProperties(addlContainerSeperator, 1, 0);
		}

		if (getEnableNotifications() != null && getEnableNotifications().getValue()) {
			componentsList.add(notificationComponent);
			componentsList.add(profileSeperatorComponent);

			toolbarConfigbuilder.addComponentLayoutProperties(notificationComponent, 25, 0, false);
			toolbarConfigbuilder.addComponentLayoutProperties(profileSeperatorComponent, 1, 0, false);
		}

		componentsList.add(userPofileMenuComponent);
		toolbarConfigbuilder.addComponentLayoutProperties(userPofileMenuComponent, 50, 0, false);

		UIContainerConfig containerConfig = new UIContainerConfig.Builder().setComponents(componentsList)
				.withLayout(toolbarConfigbuilder.build()).build();

		return containerConfig;
	}
	
	@Override
	protected ImageConfig getSolutionLogoComponent() {
		ImageConfig.Builder customerLogoBuilder = new ImageConfig.Builder().withId("solheaderCustomerLogo")
				.property(TOOLTIP, IVuxConstants.TOOLTIP_SOLUTION_LOGO).withType(EUIControlType.Image)
				.property("click", "vuxCommonUtils.openDefaultPage").property("css", "ao-vux-solution-logo")
				.property("width", 130);
		IConfigProperty<String> customLogoPath = getCustomLogoPath();
		customLogoPath.setName("label");
		customerLogoBuilder.configProperty(customLogoPath);
		ImageConfig customerLogo = customerLogoBuilder.build();

		return customerLogo;
	}
}
