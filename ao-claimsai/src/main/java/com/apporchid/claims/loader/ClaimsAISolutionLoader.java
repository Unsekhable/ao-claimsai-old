package com.apporchid.claims.loader;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.apporchid.claims.ui.builder.ClaimsAISolutionBuilder;
import com.apporchid.vulcanux.config.loader.BaseSolutionLoader;

@Profile("!solution-dev")
@Component
@DependsOn(value = { "VuxCacheLoader" })
@ComponentScan(basePackages = "com.apporchid")
public class ClaimsAISolutionLoader extends BaseSolutionLoader<ClaimsAISolutionBuilder> {
	private static final String[] DEFAULT_ACCESS_ROLES = new String[] { "administrator", "demo_user" };

	@Override
	protected Class<ClaimsAISolutionBuilder> getSolutionBuilderType() {
		return ClaimsAISolutionBuilder.class;
	}

	@Override
	protected boolean isReloadOnServerStartup() {
		return true;
	}

	@Override
	protected String[] getAccessRoles() {
		return DEFAULT_ACCESS_ROLES;
	}
}
