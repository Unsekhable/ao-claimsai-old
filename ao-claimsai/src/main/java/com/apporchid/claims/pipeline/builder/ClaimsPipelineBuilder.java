package com.apporchid.claims.pipeline.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

import com.apporchid.cloudseer.common.pipeline.tasktype.DatasinkTaskType;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasourceTaskType;
import com.apporchid.cloudseer.datasink.output.OutputDatasink;
import com.apporchid.cloudseer.datasink.output.OutputDatasinkProperties;
import com.apporchid.cloudseer.datasource.db.RelationalDBDatasource;
import com.apporchid.cloudseer.datasource.db.RelationalDBDatasourceProperties;
import com.apporchid.common.Variables;
import com.apporchid.foundation.common.IVariables;
import com.apporchid.foundation.constants.ECacheType;
import com.apporchid.foundation.pipeline.IPipeline;
import com.apporchid.foundation.pipeline.tasktype.ITaskType;
import com.apporchid.claims.constants.IClaimsAIQryConstants;
import com.apporchid.claims.constants.IClaimsAISolutionConstants;


@Component
public class ClaimsPipelineBuilder extends BaseClaimsAIPipelineConfigurationBuilder implements IClaimsAISolutionConstants, IClaimsAIQryConstants {

	@Override
	protected List<IPipeline> getPipelines() {

		List<IPipeline> pipelinesList = new ArrayList<>();
		pipelinesList.add(createPipeline("MessagePipeline", getExcelTasks("message")));

		pipelinesList.add(createPipeline("ClaimGridPipeline",
		 getExcel2DBSinkTasks("data/excel/2261_v_ClaimGrid.xlsx", "claim_grid")));

		pipelinesList.add(createPipeline("ClaimChangeLogGridPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vClaimChangeLogGrid.xlsx", "claim_change_log_grid")));
		pipelinesList.add(createPipeline("ClaimContactReserveFeatureHistoryGridPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vClaimContactReserveFeatureHistoryGrid.xlsx",
						"claim_contact_reserve_feature_history_grid")));
		pipelinesList.add(createPipeline("ClaimContactsGridPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vClaimContactsGrid.xlsx", "claim_contacts_grid")));
		pipelinesList.add(createPipeline("ClaimPaymentsGridPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vClaimPaymentsGrid.xlsx", "claim_payments_grid")));
		pipelinesList.add(createPipeline("ClaimPolicyReservesHistoryGridPipeline", getExcel2DBSinkTasks(
				"data/excel/2261_vClaimPolicyReservesHistoryGrid.xlsx", "claim_policy_reserves_history_grid")));
		pipelinesList.add(createPipeline("ClaimVehicleCoverageHistoryGridPipeline", getExcel2DBSinkTasks(
				"data/excel/2261_vClaimVehicleCoverageHistoryGrid.xlsx", "claim_vehicle_coverage_history_grid")));
		pipelinesList.add(createPipeline("ClaimVendorsGridPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vClaimVendorsGrid.xlsx", "claim_vendors_grid")));
		pipelinesList.add(createPipeline("ContactGridPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vContactGrid.xlsx", "contact_grid")));
		pipelinesList.add(
				createPipeline("FeaturesPipeline", getExcel2DBSinkTasks("data/excel/2261_vFeatures.xlsx", "features")));
		pipelinesList.add(createPipeline("IndemnityHistoryGridPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vIndemnityHistoryGrid.xlsx", "indemnity_history_grid")));
		pipelinesList.add(createPipeline("OpenFeaturesPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vOpenFeatures.xlsx", "open_features")));
		pipelinesList.add(createPipeline("PCS_ClaimsPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vPCS_Claims.xlsx", "pcs_claims")));
		pipelinesList.add(createPipeline("PCS_FeaturesPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vPCS_Features.xlsx", "pcs_features")));
		pipelinesList.add(createPipeline("PCS_PaymentTransactionsPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vPCS_PaymentTransactions.xlsx", "pcs_payment_transactions")));
		pipelinesList.add(createPipeline("UserGridPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vUserGrid.xlsx", "user_grid")));
		pipelinesList.add(createPipeline("VendorGridPipeline",
				getExcel2DBSinkTasks("data/excel/2261_vVendorGrid.xlsx", "vendor_grid")));

		pipelinesList.add(createPipeline("GetClaimChangeLogPipeline", getQueryDataTasks(QRY_GET_CLAIM_CHANGE_LOG_GRID)));
		pipelinesList.add(createPipeline("ClaimContactReserveFeatureHistoryPipeline", getQueryDataTasks(QRY_GET_CLAIM_CONTTTACT_RESERVE_FEATURE_GRID)));
		
		pipelinesList.add(createPipeline("ClaimContactGridPipeline", getQueryDataTasks(QRY_GET_CLAIM_CONTTTACT_GRID)));
		pipelinesList.add(createPipeline("ClaimGridPipeline", getQueryDataTasks(QRY_GET_CLAIM_GRID)));
		pipelinesList.add(createPipeline("ClaimPaymentGridPipeline", getQueryDataTasks(QRY_GET_CLAIM_PAYMENT_GRID)));
		pipelinesList.add(createPipeline("ClaimVehicleCoverageHistoryPipeline", getQueryDataTasks(QRY_GET_CLAIM_VEHICLE_COVERAGE_HISTORY_GRID)));
		pipelinesList.add(createPipeline("ClaimVendorsPipeline", getQueryDataTasks(QRY_GET_CLAIM_VENDORS_GRID)));
		pipelinesList.add(createPipeline("ContactGridPipeline", getQueryDataTasks(QRY_GET_CONTACT_GRID)));
		pipelinesList.add(createPipeline("FeaturesPipeline", getQueryDataTasks(QRY_GET_FEATURES)));
		pipelinesList.add(createPipeline("VendorGridPipeline", getQueryDataTasks(QRY_GET_VENDOR_GRID)));
		return pipelinesList;
	}
	
	
	private ITaskType<?, ?>[] getExcelTasks(String fileName) {
		return getExcelTasks(fileName, null);
	}
	
	private ITaskType<?, ?>[] getExcel2DBSinkTasks(String excelFilePath, String tableName) {
		return getExcel2DBSinkTasks(excelFilePath, tableName, null);
	}
	
	private ITaskType<?, ?>[] getExcel2DBSinkTasks(String excelFilePath, String tableName,
			Map<String, Object> addlProps) {
		List<ITaskType<?, ?>> pipelineTasks = new ArrayList<>();
		pipelineTasks.add(fileTaskBuilderHelper().getExcelTask(excelFilePath, false, addlProps));
		pipelineTasks.add(dbTaskBuilderHelper().getDBDatasink(tableName));

		return pipelineTasks.toArray(new ITaskType<?, ?>[0]);
	}
	

	private ITaskType<?, ?>[] getExcelTasks(String fileName, Map<String, Object> additionalProps) {
		return fileTaskBuilderHelper().getExcelTasks("data/excel/" + fileName + ".xlsx", false, additionalProps);
	}

	public static ITaskType<?, ?>[] getQueryDataTasks(String query) {
		DatasourceTaskType sourceTask = new DatasourceTaskType.Builder().name("Read data")
				.datasourceType(RelationalDBDatasource.class)
				.datasourcePropertiesType(RelationalDBDatasourceProperties.class)
				.property(RelationalDBDatasourceProperties.EProperty.sqlDatasourceName.name(), DATASOURCE_NAME)
				.property(RelationalDBDatasourceProperties.EProperty.sqlQuery.name(), query)
				.build();

		DatasinkTaskType sinkTask = new DatasinkTaskType.Builder().name("Write data").datasinkType(OutputDatasink.class)
				.datasinkPropertiesType(OutputDatasinkProperties.class).build();
		ITaskType<?, ?>[] tasks = { sourceTask, sinkTask };
		return tasks;
	}
	
	
	public ITaskType<?, ?>[] getESAutoCompletePipelineTasks() {
		// return esTaskBuilderHelper().getESAutoCompleteTasks("cloudseer-analytics",
		// ES_OSHA_INDEX_NAME, ES_OSHA_TYPE_NAME);
		return esTaskBuilderHelper().getESAutoCompleteTasks("cloudseer-analytics",null);
	}

	@Override
	protected IVariables getInputVariables(String pipelineName, int index) {
		IVariables inputVariables = new Variables();
		switch (index) {
		case 0:
			inputVariables.add("msoDirectoryPath", "/claims/mso/");
			break;
		default:
			return null;
		}
		return inputVariables;

	}

	@Override
	protected ECacheType getCacheType(String pipelineName) {
		if (pipelineName.contains("EventsPivotPipeline")) {
			return ECacheType.GLOBAL_CACHE;
		}
		return super.getCacheType(pipelineName);
	}
	
}
