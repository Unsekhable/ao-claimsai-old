package com.apporchid.claims.constants;

public interface IClaimsAISolutionConstants {
	
	public static final String DEFAULT_DOMAIN_ID = "com.apporchid";
	public static final String DEFAULT_SUB_DOMAIN_ID = "claims";
	public static final String SOLUTION_ID = "claims";
	public static final String SOLUTION_NAME = "AppOrchid ClaimsAI";
	public static final String IMAGES_LOCATION = "/main-ui/solutions/" + SOLUTION_ID;

	public static final String SOLUTION_LOGO = IMAGES_LOCATION + "/images/demo-logo.png";
	public static final String SOLUTION_ICON = IMAGES_LOCATION + "/images/demo-icon.png";

	public static final String DATASOURCE_NAME = "claimsAIImplDB";
	public static final String DEFAULT_ES_CLUSTER_NAME = "cloudseer-analytics";
	

}
