package com.apporchid.claims.constants;

public interface IClaimsAIQryConstants {
	
	public static final String QRY_GET_CLAIM_CHANGE_LOG_GRID = "SELECT * FROM public.claim_change_log_grid";
	public static final String QRY_GET_CLAIM_CONTTTACT_RESERVE_FEATURE_GRID = "SELECT * FROM public.claim_contact_reserve_feature_history_grid";
	
	public static final String QRY_GET_CLAIM_CONTTTACT_GRID = "SELECT * FROM public.claim_contacts_grid";
	public static final String QRY_GET_CLAIM_GRID = "SELECT * FROM public.claim_grid";
	public static final String QRY_GET_CLAIM_PAYMENT_GRID = "SELECT * FROM public.claim_payments_grid";
	public static final String QRY_GET_CLAIM_VEHICLE_COVERAGE_HISTORY_GRID = "SELECT * FROM public.claim_vehicle_coverage_history_grid";
	public static final String QRY_GET_CLAIM_VENDORS_GRID = "SELECT * FROM public.claim_vendors_grid";
	public static final String QRY_GET_CONTACT_GRID = "SELECT * FROM public.contact_grid";
	public static final String QRY_GET_FEATURES = "SELECT * FROM public.features";
	public static final String QRY_GET_VENDOR_GRID = "SELECT * FROM public.vendor_grid";
	
	
}
