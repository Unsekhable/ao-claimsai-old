package com.apporchid.solution.test;

import org.testng.annotations.Test;

import com.apporchid.claims.constants.IClaimsAISolutionConstants;
import com.apporchid.claims.ui.builder.ClaimsAISolutionBuilder;
import com.apporchid.solution.common.BaseSolutionTestApplication;


public class ClaimsAISolutionLoaderTest extends BaseSolutionTestApplication implements IClaimsAISolutionConstants{

	@Test(groups = { "solutionMain" })
	@Override
	public void setupSolution() {
		//deploy everything
		deploy(ClaimsAISolutionBuilder.class);

		//deploy only pipelines
		//deploy(ClaimsAIPipelineBuilder.class);

		//run all pipeline for aspire Data  
		//runPipeline("ClaimGridPipeline");
		//runPipeline("ClaimChangeLogGridPipeline");
		//runPipeline("ClaimContactReserveFeatureHistoryGridPipeline");
		//runPipeline("ClaimContactsGridPipeline");
		//runPipeline("ClaimPaymentsGridPipeline");
		//runPipeline("ClaimPolicyReservesHistoryGridPipeline");
		//runPipeline("ClaimVehicleCoverageHistoryGridPipeline");
		//runPipeline("ClaimVendorsGridPipeline");
		//runPipeline("ContactGridPipeline");
		//runPipeline("FeaturesPipeline");
		//runPipeline("IndemnityHistoryGridPipeline");
		//runPipeline("OpenFeaturesPipeline");
		//runPipeline("PCS_ClaimsPipeline");
		//runPipeline("PCS_FeaturesPipeline");
		//runPipeline("PCS_PaymentTransactionsPipeline");
		//runPipeline("UserGridPipeline");
		//runPipeline("VendorGridPipeline");
		
		//runPipeline("GetClaimChangeLogPipeline");
		
	}

	@Override
	protected String getSolutionPackage() {
		return "com.apporchid";
	}
	@Override
	protected String getDomainId() {
		return DEFAULT_DOMAIN_ID;
	}

	@Override
	protected String getSubDomainId() {
		return DEFAULT_SUB_DOMAIN_ID;
	}
}
